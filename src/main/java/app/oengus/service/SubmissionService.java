package app.oengus.service;

import app.oengus.dao.CategoryRepository;
import app.oengus.entity.dto.AvailabilityDto;
import app.oengus.entity.dto.OpponentCategoryDto;
import app.oengus.entity.dto.OpponentSubmissionDto;
import app.oengus.entity.model.*;
import app.oengus.exception.OengusBusinessException;
import app.oengus.helper.OengusConstants;
import app.oengus.service.repository.MarathonRepositoryService;
import app.oengus.service.repository.SelectionRepositoryService;
import app.oengus.service.repository.SubmissionRepositoryService;
import app.oengus.service.repository.UserRepositoryService;
import app.oengus.spring.model.Role;
import javassist.NotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SubmissionService {

	@Autowired
	private SubmissionRepositoryService submissionRepositoryService;

	@Autowired
	private MarathonRepositoryService marathonRepositoryService;

	@Autowired
	private SelectionRepositoryService selectionRepositoryService;

	@Autowired
	private UserRepositoryService userRepositoryService;

	@Autowired
	private CategoryRepository categoryRepository;

	private final List<RunType> MULTIPLAYER_RUN_TYPES = List.of(RunType.COOP, RunType.COOP_RACE, RunType.RACE);

	@Transactional
	public Submission save(final Submission submission, final User submitter, final String marathonId)
			throws NotFoundException {
		final Marathon marathon =
				this.marathonRepositoryService.findById(marathonId);
		submission.setUser(submitter);
		submission.setMarathon(marathon);
		submission.getAvailabilities().forEach(availability -> {
			availability.setFrom(availability.getFrom().withSecond(0));
			availability.setTo(availability.getTo().withSecond(0));
		});
		submission.getGames().forEach(game -> {
			game.setSubmission(submission);
			game.getCategories().forEach(category -> {
				category.setGame(game);
				if (category.getId() == null) {
					this.createSelection(category, marathon);
				} else {
					final Selection selection = this.selectionRepositoryService.findByCategory(category);
					if (selection == null) {
						this.createSelection(category, marathon);
					}
					category.setSelection(selection);
				}
				if (category.getEstimate().toSecondsPart() > 0) {
					category.setEstimate(category.getEstimate().plusMinutes(1).truncatedTo(ChronoUnit.MINUTES));
				}
				if (this.MULTIPLAYER_RUN_TYPES.contains(category.getType())) {
					if (StringUtils.isEmpty(category.getCode())) {
						String code;
						do {
							code = RandomStringUtils.random(6, true, true).toUpperCase();
						} while (this.categoryRepository.existsByCode(code));
						category.setCode(code);
					}
				} else {
					category.setCode(null);
				}
			});
		});
		submission.getAnswers().forEach(answer -> answer.setSubmission(submission));
		submission.setOpponents(new HashSet<>());
		if (submission.getOpponentDtos() != null) {
			submission.getOpponentDtos().forEach(opponentDto -> {
				final Opponent opponent = new Opponent();
				opponent.setId(opponentDto.getId());
				opponent.setSubmission(submission);
				opponent.setVideo(opponentDto.getVideo());
				final Category category = new Category();
				category.setId(opponentDto.getCategoryId());
				opponent.setCategory(category);
				submission.getOpponents().add(opponent);
			});
		}
		return this.submissionRepositoryService.save(submission);
	}

	public Map<String, List<AvailabilityDto>> getRunnersAvailabilitiesForMarathon(final String marathonId) {
		final Marathon marathon = new Marathon();
		marathon.setId(marathonId);
		final List<Submission> submissions =
				this.submissionRepositoryService.findValidatedOrBonusSubmissionsForMarathon(marathon);
		final Map<String, List<AvailabilityDto>> availabilities = new HashMap<>();
		submissions.forEach(submission -> {
			final List<AvailabilityDto> availabilityDtoList = new ArrayList<>();
			submission.getAvailabilities().forEach(availability -> {
				final AvailabilityDto availabilityDto =
						new AvailabilityDto(submission.getUser().getUsername(),
								submission.getUser().getUsername("ja"));
				availabilityDto.setFrom(availability.getFrom());
				availabilityDto.setTo(availability.getTo());
				availabilityDtoList.add(availabilityDto);
			});
			availabilities.put(submission.getUser().getUsername(), availabilityDtoList);
			submission.getGames().forEach(game -> {
				game.getCategories().forEach(category -> {
					if (OengusConstants.ACCEPTED_STATUSES.contains(category.getSelection().getStatus())) {
						category.getOpponents().forEach(opponent -> {
							final List<AvailabilityDto> opponentAvailabilityDtoList = new ArrayList<>();
							opponent.getSubmission().getAvailabilities().forEach(availability -> {
								final AvailabilityDto availabilityDto =
										new AvailabilityDto(opponent.getSubmission().getUser().getUsername(),
												opponent.getSubmission().getUser().getUsername("ja"));
								availabilityDto.setFrom(availability.getFrom());
								availabilityDto.setTo(availability.getTo());
								opponentAvailabilityDtoList.add(availabilityDto);
							});
							availabilities.put(opponent.getSubmission().getUser().getUsername(),
									opponentAvailabilityDtoList);
						});
					}
				});
			});
		});
		return availabilities;
	}

	@Transactional
	public Map<String, List<AvailabilityDto>> getRunnerAvailabilitiesForMarathon(final String marathonId,
	                                                                             final Integer runnerId)
			throws NotFoundException {
		final Marathon marathon = new Marathon();
		marathon.setId(marathonId);
		final User user =
				this.userRepositoryService.findById(runnerId);
		final Submission submission = this.submissionRepositoryService.findByUserAndMarathon(user, marathon);
		final Map<String, List<AvailabilityDto>> availabilities = new HashMap<>();
		final List<AvailabilityDto> availabilityDtoList = new ArrayList<>();
		if (submission != null) {
			submission.getAvailabilities().forEach(availability -> {
				final AvailabilityDto availabilityDto =
						new AvailabilityDto(user.getUsername(), user.getUsername("ja"));
				availabilityDto.setFrom(availability.getFrom());
				availabilityDto.setTo(availability.getTo());
				availabilityDtoList.add(availabilityDto);
			});
		}
		availabilities.put(user.getUsername(), availabilityDtoList);
		return availabilities;
	}

	private void createSelection(final Category category, final Marathon marathon) {
		final Selection selection = new Selection();
		selection.setStatus(Status.TODO);
		selection.setMarathon(marathon);
		selection.setCategory(category);
		category.setSelection(selection);
	}

	@Transactional
	public Submission findByUserAndMarathon(final User user, final String marathonId) {
		final Marathon marathon = new Marathon();
		marathon.setId(marathonId);
		final Submission submission = this.submissionRepositoryService.findByUserAndMarathon(user, marathon);
		if (submission != null) {
			if (submission.getOpponents() != null) {
				submission.setOpponentDtos(new HashSet<>());
				submission.getOpponents().forEach(opponent -> {
					submission.getOpponentDtos().add(this.mapOpponent(opponent, user));
				});
			}
			if (submission.getGames() != null) {
				submission.getGames().forEach(game -> {
					game.getCategories().forEach(category -> {
						if (category.getOpponents() != null) {
							category.setOpponentDtos(new ArrayList<>());
							category.getOpponents().forEach(opponent -> {
								final OpponentCategoryDto opponentCategoryDto = new OpponentCategoryDto();
								opponentCategoryDto.setId(opponent.getId());
								opponentCategoryDto.setVideo(opponent.getVideo());
								opponentCategoryDto.setUser(opponent.getSubmission().getUser());
								category.getOpponentDtos().add(opponentCategoryDto);
							});
						}
					});
				});
			}
		}
		return submission;
	}

	private OpponentSubmissionDto mapOpponent(final Opponent opponent, final User user) {
		final OpponentSubmissionDto opponentDto = new OpponentSubmissionDto();
		opponentDto.setId(opponent.getId());
		opponentDto.setGameName(opponent.getCategory().getGame().getName());
		opponentDto.setCategoryId(opponent.getCategory().getId());
		opponentDto.setCategoryName(opponent.getCategory().getName());
		opponentDto.setVideo(opponent.getVideo());
		final List<User> users = new ArrayList<>();
		users.add(opponent.getCategory().getGame().getSubmission().getUser());
		users.addAll(opponent.getCategory()
		                     .getOpponents()
		                     .stream()
		                     .map(opponent1 -> opponent1.getSubmission().getUser())
		                     .filter(user1 -> !Objects.equals(user1.getId(), user.getId()))
		                     .collect(
				                     Collectors.toSet()));
		opponentDto.setUsers(users);
		return opponentDto;
	}

	@Transactional
	public List<Submission> findByMarathon(final String marathonId) {
		final Marathon marathon = new Marathon();
		marathon.setId(marathonId);
		return this.submissionRepositoryService.findByMarathon(marathon);
	}

	@Transactional
	public List<Submission> findCustomAnswersByMarathon(final String marathonId) {
		final Marathon marathon = new Marathon();
		marathon.setId(marathonId);
		return this.submissionRepositoryService.findCustomAnswersByMarathon(marathon);
	}

	@Transactional
	public void deleteByMarathon(final Marathon marathon) {
		this.submissionRepositoryService.deleteByMarathon(marathon);
	}

	public Boolean userHasSubmitted(final Marathon marathon, final User user) {
		return this.submissionRepositoryService.existsByMarathonAndUser(marathon, user);
	}

	public void delete(final Integer id, final User user) throws NotFoundException {
		final Submission submission = this.submissionRepositoryService.findById(id);
		final Marathon marathon = submission.getMarathon();
		if (submission.getUser().getId().equals(user.getId()) || user.getRoles().contains(Role.ROLE_ADMIN) ||
				marathon.getCreator().getId().equals(user.getId()) ||
				marathon.getModerators().stream().anyMatch(u -> u.getId().equals(user.getId()))) {
			this.submissionRepositoryService.delete(id);
		} else {
			throw new OengusBusinessException("NOT_AUTHORIZED");
		}
	}
}
